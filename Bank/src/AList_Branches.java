import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by bouh on 12/01/17.
 */
public class AList_Branches {
    public ArrayList<Branch> list;

    /*
    *** Constructors
    *** ------------
     */

    public AList_Branches (ArrayList<Branch> list){
        this.list = list;
    }

    public AList_Branches () {
        this(new ArrayList<>());
    }

    /*
    *** Methods
    *** -------
     */

    /*
    *** list
     */

    public Branch find(String name){
        for (Branch element : this.list){
            if (Objects.equals(element.getName(), name)){
                return element;
            }
        }
        return null;
    }

    public void add(Branch element){
        this.list.add(element);
    }

    public void create(String name){
        this.list.add(new Branch(name));
    }

    public void remove(Branch element){
        this.list.remove(element);
    }

    public void remove(int id){
        this.list.remove(id);
    }

    public void remove(String name){
        this.list.remove(find(name));
    }

    public String toStringList() {
        String msg = "";
        for (Branch element : this.list){
            msg += element.toString();
        }
        return msg;
    }
}
