import java.util.Scanner;

/**
 * Created by bouh on 12/01/17.
 */
public class Main {

    public static String prompt(String msg) {
        Scanner scanner = new Scanner(System.in);

        System.out.print(msg + "\n");
        return scanner.nextLine();
    }

    public static void main(String[] args) {
        Bank bank = new Bank();

        bank.createBranch("Europe");
        bank.createBranch("NorthAmerica");
        bank.createBranch("Asia");
        System.out.println(bank.toStringBranchesList());
        
        prompt("Press enter");

        bank.createCustomer("Europe", "Jérémy");
        bank.createCustomer("Europe", "Louise");
        bank.createCustomer("NorthAmerica", "Tom");
        bank.createCustomer("NorthAmerica", "Julie");
        bank.createCustomer("Asia", "name1");
        bank.createCustomer("Asia", "name2");
        System.out.println(bank.toStringBranchesList());

        prompt("Press enter");
        bank.createTransaction("Europe", "Jérémy", "Europe", "Louise", 100);
        bank.createTransaction("Europe", "Louise", "Asia", "name1", 1000);
        bank.createTransaction("NorthAmerica", "Tom", "Europe", "Jérémy", 10);
        System.out.println(bank.toStringTransactionsList());
        prompt("Press enter");
        System.out.println(bank.toStringBranchesList());
    }
}
