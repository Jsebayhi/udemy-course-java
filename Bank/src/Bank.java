/**
 * Created by bouh on 12/01/17.
 */
public class Bank {
    public AList_Branches branches;
    public AList_Transactions transactions;

    /*
    *** Constructors
    *** ------------
     */

    public Bank(AList_Branches branches, AList_Transactions transactions) {
        this.branches = branches;
        this.transactions = transactions;
    }

    public Bank() {
        this(new AList_Branches(), new AList_Transactions());
    }

    /*
    *** Methods
    *** -------
     */


    /*
    *** Branches
     */

    public void createBranch(String Name){
        this.branches.create(Name);
    }

    public void removeBranch(String Name){
        this.branches.remove(Name);
    }

    public String toStringBranchesList(){
        return branches.toStringList();
    }


    /*
    *** Transactions
     */

    public boolean createTransaction(String CF_branch, String CF_name, String CT_branch, String CT_name, double amount){
        Customer customerFrom = branches.find(CF_branch).find(CF_name);
        Customer customerTo = branches.find(CT_branch).find(CT_name);
        if (customerFrom == null || customerTo == null || amount < 0){
            return false;
        }
        this.transactions.create(customerFrom, customerTo, amount);
        return true;
    }

    public void createTransaction(Customer customerFrom, Customer customerTo, double amount){
        this.transactions.create(customerFrom, customerTo, amount);
    }

    public String toStringTransactionsList(){
        return transactions.toStringList();
    }

    /*
    *** Customers
     */

    public void addCustomer(Branch branch, Customer customer) {
        branch.addCustomer(customer);
    }

    public void addCustomer(String branch, Customer customer) {
        addCustomer(this.branches.find(branch), customer);
    }

    public void createCustomer(String Branch, String Name){
        this.branches.find(Branch).createCustomer(Name);
    }

    public void removeCustomer(String Branch, String Name){
        this.branches.find(Branch).removeCustomer(Name);
    }

}
