/**
 * Created by bouh on 12/01/17.
 */
public class Customer {
	private String name;
	private double credit;


    /*
    *** Constructors
    *** ------------
     */

    public Customer(String name, double credit) {
        this.name = name;
        this.credit = credit;
    }

    public Customer(String name) {
        this(name, 0);
    }

    /*
    *** Methods
    *** -------
     */

    public String toString(){
        return getName() + " Credits: " + getCredit();
    }

    /*
    *** Name
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*
    *** Credit
     */

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public void subCredit(double credit){
        this.credit -= credit;
    }

    public void addCredit(double credit){
        this.credit += credit;
    }
}
