/**
 * Created by bouh on 12/01/17.
 */
public class Transaction {
	public Customer customer_From;
	public Customer customer_To;
	/*
	*** Must be positive.
	 */
	private double amount;

	/*
    *** Constructors
    *** ------------
     */

    public Transaction(Customer customer_From, Customer customer_To, double amount) {
        this.customer_From = customer_From;
        this.customer_To = customer_To;
        this.setAmount(amount);
        customer_From.subCredit(this.amount);
        customer_To.addCredit(this.amount);
    }

    /*
    *** Methods
    *** -------
     */

    /*
    *** Amount
     */

    public double getAmount() {
        return amount;
    }

    /*
    *** Check if amount positive, if not set amount to 0.
     */
    public void setAmount(double amount) {
        if (amount >= 0) {
            this.amount = amount;
        } else {
            this.amount = 0;
        }
    }
}
