import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by bouh on 12/01/17.
 */
public class AList_Transactions {
    ArrayList<Transaction> list;

    /*
    *** Constructors
    *** ------------
     */

    public AList_Transactions (ArrayList<Transaction> transactions){
        this.list = transactions;
    }

    public AList_Transactions () {
        this(new ArrayList<>());
    }

    /*
    *** Methods
    *** -------
     */

    /*
    *** list
     */

    public Transaction findByFrom(String name){
        for (Transaction element : this.list){
            if (Objects.equals(element.customer_From.getName(), name)){
                return element;
            }
        }
        return null;
    }

    public Transaction findByTo(String name){
        for (Transaction element : this.list){
            if (Objects.equals(element.customer_To.getName(), name)){
                return element;
            }
        }
        return null;
    }


    public void add(Transaction element){
        this.list.add(element);
    }

    public void create(Customer from, Customer to, double amount){
        this.list.add(new Transaction(from, to, amount));
    }

    public void remove(Transaction element){
        this.list.remove(element);
    }

    public void remove(int id){
        this.list.remove(id);
    }

    public String toStringList(){
        String msg = "";
        for (Transaction element : this.list){
            msg += " From: " + element.customer_From.getName() + " To: " + element.customer_To.getName() + " Amount: " + element.getAmount() + "\n";
        }
        return msg;
    }

}
