import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by bouh on 12/01/17.
 */
public class AList_Customers {
    public ArrayList<Customer> list;

    /*
    *** Constructors
    *** ------------
     */

    public AList_Customers (ArrayList<Customer> Customers){
        this.list = Customers;
    }

    public AList_Customers () {
        this(new ArrayList<>());
    }

    /*
    *** Methods
    *** -------
     */

    /*
    *** list
     */

    public Customer find(String name){
        for (Customer element : this.list){
            if (Objects.equals(element.getName(), name)){
                return element;
            }
        }
        return null;
    }

    public void add(Customer element){
        this.list.add(element);
    }

    public void create(String name){
        this.list.add(new Customer(name));
    }

    public void remove(Customer element){
        this.list.remove(element);
    }

    public void remove(int id){
        this.list.remove(id);
    }

    public String toStringList(){
        String msg = "";
        for (Customer element : this.list){
            msg += element.toString() + "\n";
        }
        return msg;
    }

}
