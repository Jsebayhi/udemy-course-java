/**
 * Created by bouh on 12/01/17.
 */
public class Branch {

    private String name;
    public AList_Customers customers;

    /*
    *** Constructors
    *** ------------
     */

    public Branch(String name, AList_Customers customers) {
        this.name = name;
        this.customers = customers;
    }

    public Branch(String name) {
        this(name, new AList_Customers());
    }

    public Branch() {
        this("NotSpecified");
    }

    /*
    *** Methods
    *** -------
     */

    public String toString(){
        return "--" + this.getName() + "\n" + this.customers.toStringList();
    }

    /*
    *** Name
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*
    *** Customers
     */

    public AList_Customers getCustomers() {
        return customers;
    }

    public void setCustomers(AList_Customers customers) {
        this.customers = customers;
    }

    public void addCustomer(Customer customer) {
        this.customers.add(customer);
    }

    public void createCustomer(String name) {
        this.customers.add(new Customer(name));
    }

    public void removeCustomer(String name) {
        this.customers.remove(this.customers.find(name));
    }

    public Customer find(String name) {
        return this.customers.find(name);
    }
}
