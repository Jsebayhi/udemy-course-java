import java.util.ArrayList;
import java.util.Scanner;
import java.util.Objects;

/**
 * Created by bouh on 10/01/17.
 */
public class Main {

    public static String prompt(String msg) {
        Scanner scanner = new Scanner(System.in);

        System.out.print(msg + "\n");
        return scanner.nextLine();
    }

    public static ArrayList<food> foodsIni() {
        ArrayList<food> foods;
        foods = new ArrayList<food>();
        foods.add(new food("pain de mie", 0.20));
        foods.add(new food("pain complet", 0.20));
        foods.add(new food("steak", 0.40));
        foods.add(new food("carotte", 0.20));
        foods.add(new food("laitue", 0.20));
        foods.add(new food("oignons", 0.20));
        foods.add(new food("tomates", 0.20));
        foods.add(new food("chips", 1.00));
        foods.add(new food("stickyCarotte", 1.00));
        foods.add(new food("soda", 1.00));
        foods.add(new food("soda2", 1.00));
        return foods;
    }

    public static food getFoodByName(ArrayList<food> foods, String name) {
        for (int element_id = 0; element_id < foods.size(); element_id++) {
            if (Objects.equals(foods.get(element_id).getName(), name)) {
                return foods.get(element_id);
            }
        }
        return null;
    }

    public static ArrayList<Burger_type> burger_typeIni(ArrayList<food> foods) {
        ArrayList<food> content;
        ArrayList<food> additionsAvailable;
        ArrayList<Burger_type> burger_types;

        burger_types = new ArrayList<Burger_type>();

        additionsAvailable = new ArrayList<food>();
        content = new ArrayList<food>();
        content.add(getFoodByName(foods, "pain de mie"));
        content.add(getFoodByName(foods, "steak"));
        additionsAvailable.add(getFoodByName(foods, "carotte"));
        additionsAvailable.add(getFoodByName(foods, "laitue"));
        additionsAvailable.add(getFoodByName(foods, "oignons"));
        additionsAvailable.add(getFoodByName(foods, "tomates"));
        burger_types.add(new Burger_type("CheeseBurger", content, 0, additionsAvailable, 4));

        additionsAvailable = new ArrayList<food>();
        content = new ArrayList<food>();
        content.add(getFoodByName(foods, "pain complet"));
        content.add(getFoodByName(foods, "steak"));
        content.add(getFoodByName(foods, "laitue"));
        additionsAvailable.add(getFoodByName(foods, "carotte"));
        additionsAvailable.add(getFoodByName(foods, "laitue"));
        additionsAvailable.add(getFoodByName(foods, "oignons"));
        additionsAvailable.add(getFoodByName(foods, "tomates"));
        burger_types.add(new Burger_type("Hamburger", content, 0, additionsAvailable, 2));

        return burger_types;
    }

    public static void main(String[] args) {
        //foods
        ArrayList<food> foods;
        foods = foodsIni();

        //burger_type
        ArrayList<Burger_type> burger_types;
        burger_types = burger_typeIni(foods);

        //burger
        ArrayList<Burger> burgers;

        //programs
        String input;
        Scanner scanner = new Scanner(System.in);
        int element_id;
        int tmp;

        while (true) {
            System.out.print("NOTE: THERE IS NO USER ERROR HANDLING\n");
            System.out.print("0] Exit\n");
            System.out.print("1] Admin\n");
            System.out.print("2] Client\n");
            input = scanner.nextLine();
            if (Objects.equals(input, "0")) {
                break;
            } else if (Objects.equals(input, "1")) {
                /*
                ** ADMIN HANDLER
                 */
                while (true) {
                    System.out.print("0] Exit\n");
                    System.out.print("1] Modify foods\n");
                    System.out.print("2] Modify Burgers type\n");
                    input = scanner.nextLine();
                    if (Objects.equals(input, "0")) {
                        break;
                    } else if (Objects.equals(input, "1")) {
                        /*
                        *** Modify foods
                         */
                        for (element_id = 0; element_id < foods.size(); element_id++) {
                            System.out.print("" + element_id + "] " + foods.get(element_id) + "\n");
                        }
                        System.out.print(element_id++ + "] Add\n");
                        //System.out.print(tmp++ + "] Add food\n");
                        input = scanner.nextLine();
                        element_id = Integer.parseInt(input);
                        if (element_id >= foods.size()) {
                            if (Objects.equals(input, Integer.toString(foods.size()))) {
                                foods.add(new food(prompt("Enter new name:"), Double.parseDouble(prompt("Enter new price:"))));
                                System.out.print("Foods item is as been removed.\n");
                            }
                        } else {
                            while (true) {
                                System.out.print("0] Exit\n");
                                System.out.print("1] Change Name\n");
                                System.out.print("2] Change price\n");
                                System.out.print("3] Delete\n");
                                input = scanner.nextLine();
                                if (Objects.equals(input, "0")) {
                                    break ;
                                } else if (Objects.equals(input, "1")) {
                                    foods.get(element_id).setName(prompt("Enter new name:"));
                                    System.out.print("Foods item is now: " + foods.get(element_id) + "\n");
                                } else if (Objects.equals(input, "2")) {
                                    foods.get(element_id).setPrice(Double.parseDouble(prompt("Enter new price:")));
                                    System.out.print("Foods item is now: " + foods.get(element_id) + "\n");
                                } else if (Objects.equals(input, "3")) {
                                    foods.remove(foods.get(element_id));
                                    System.out.print("Foods item is as been removed.\n");
                                }
                            }
                        }
                    } else if (Objects.equals(input, "2")) {
                        /*
                        *** Modify Burgers type
                         */
                        while (true) {
                            for (element_id = 0; element_id < burger_types.size(); element_id++) {
                                System.out.print("" + element_id + "] " + burger_types.get(element_id) + "\n");
                            }
                            System.out.print(element_id++ + "] Add\n");
                            System.out.print(element_id++ + "] Exit\n");
                            element_id = Integer.parseInt(scanner.nextLine());
                            if (element_id > burger_types.size()) {
                                if (element_id == burger_types.size()) {
                                    burger_types.add(new Burger_type(prompt("Enter the name of the new burger:")));
                                } else if (element_id == burger_types.size() + 1) {
                                    break;
                                }

                            } else {
                                while (true) {
                                    System.out.print(burger_types.get(element_id) + "\n");
                                    System.out.print("0] Exit\n");
                                    System.out.print("1] Change Name\n");
                                    System.out.print("2] Add content\n");
                                    System.out.print("3] Remove content\n");
                                    System.out.print("4] Add additions availables\n");
                                    System.out.print("5] Remove additions availables\n");
                                    System.out.print("6] Change price\n");
                                    System.out.print("7] Delete\n");
                                    input = scanner.nextLine();
                                    if (Objects.equals(input, "0")) {
                                        break;
                                    } else if (Objects.equals(input, "1")) {// change name
                                        burger_types.get(element_id).setName(prompt("Enter new name:"));
                                        System.out.print("burger type item is now: " + burger_types.get(element_id) + "\n");
                                    } else if (Objects.equals(input, "2")) {// add content
                                        System.out.print("Here is the list of the available foods element you can add.\n");
                                        for (int i = 0; i < foods.size(); i++) {
                                            System.out.print("" + i + "] " + foods.get(i) + "\n");
                                        }
                                        input = scanner.nextLine();
                                        burger_types.get(element_id).AddContent(foods.get(Integer.parseInt(input)));
                                        System.out.print("burger type item is now: " + burger_types.get(element_id) + "\n");
                                    } else if (Objects.equals(input, "3")) {// remove content
                                        System.out.print("Here is the list of the available foods element you can add.\n");
                                        for (int i = 0; i < burger_types.get(element_id).getContent().size(); i++) {
                                            System.out.print("" + i + "] " + burger_types.get(element_id).getContent().get(i) + "\n");
                                        }
                                        input = scanner.nextLine();
                                        burger_types.get(element_id).RemoveContent(burger_types.get(element_id).getContent().get(Integer.parseInt(input)));
                                        System.out.print("burger type item is now: " + burger_types.get(element_id) + "\n");
                                    } else if (Objects.equals(input, "4")) {// add additions availables
                                        System.out.print("Here is the list of the available foods element you can add.\n");
                                        for (int i = 0; i < foods.size(); i++) {
                                            System.out.print("" + i + "] " + foods.get(i) + "\n");
                                        }
                                        input = scanner.nextLine();
                                        burger_types.get(element_id).AddContent(foods.get(Integer.parseInt(input)));
                                        System.out.print("burger type item is now: " + burger_types.get(element_id) + "\n");
                                    } else if (Objects.equals(input, "5")) {// remove additions availables
                                        System.out.print("Here is the list of the additions availables foods elements.\n");
                                        for (int i = 0; i < burger_types.get(element_id).getAdditionsAvailable().size(); i++) {
                                            System.out.print("" + i + "] " + burger_types.get(element_id).getAdditionsAvailable().get(i) + "\n");
                                        }
                                        input = scanner.nextLine();
                                        burger_types.get(element_id).RemoveAdditionsAvailable(burger_types.get(element_id).getAdditionsAvailable().get(Integer.parseInt(input)));
                                        System.out.print("burger type item is now: " + burger_types.get(element_id) + "\n");
                                    } else if (Objects.equals(input, "6")) {// Change price
                                        System.out.print("Current base price is: " + burger_types.get(element_id).getPrice() + "\n");
                                        input = prompt("Enter new base price:");
                                        burger_types.get(element_id).setPrice(Integer.parseInt(input));
                                        System.out.print("burger type item is now: " + burger_types.get(element_id) + "\n");
                                    } else if (Objects.equals(input, "7")) {// Delete
                                        burger_types.remove(burger_types.get(element_id));
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (input == "1") {
            /*
            ** CLIENT HANDLER
             */
                while (true) {
                    input = scanner.nextLine();
                }
            }
        }
    }
}
