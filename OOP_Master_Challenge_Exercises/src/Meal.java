import java.util.ArrayList;

/**
 * Created by bouh on 10/01/17.
 */
public class Meal {
    private ArrayList<food> condiments;
    private ArrayList<food> condimentsSelected;
    private ArrayList<food> drinks;
    private ArrayList<food> drinksSelected;

    public ArrayList<food> getCondiments() {
        return condiments;
    }

    public Meal() {
        this.condiments = new ArrayList<>();
        this.condimentsSelected = new ArrayList<>();
        this.drinks = new ArrayList<>();
        this.drinksSelected = new ArrayList<>();
    }

    public void setCondiments(ArrayList<food> condiments) {
        this.condiments = condiments;
    }

    public ArrayList<food> getCondimentsSelected() {
        return condimentsSelected;
    }

    public void setCondimentsSelected(ArrayList<food> condimentsSelected) {
        this.condimentsSelected = condimentsSelected;
    }

    public ArrayList<food> getDrinks() {
        return drinks;
    }

    public void setDrinks(ArrayList<food> drinks) {
        this.drinks = drinks;
    }

    public ArrayList<food> getDrinksSelected() {
        return drinksSelected;
    }

    public void setDrinksSelected(ArrayList<food> drinksSelected) {
        this.drinksSelected = drinksSelected;
    }
}
