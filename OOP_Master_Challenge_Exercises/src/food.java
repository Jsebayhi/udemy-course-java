/**
 * Created by bouh on 10/01/17.
 */
public class food {
    private String name;
    private double price;

    public food(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public food(String name) {
        this(name, 0);
    }

    public food(double price) {
        this("NotSpecified", price);
    }

    public food() {
        this("NotSpecified", 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return (this.name + ": " + this.price);
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price >= 0) {
            this.price = price;
        }
    }
}
