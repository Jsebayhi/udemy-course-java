import java.util.ArrayList;

/**
 * Created by bouh on 10/01/17.
 */
public class Burger_type {
    private String name;
    private ArrayList<food> content;
    private double price;
    private ArrayList<food> additionsAvailable;
    private int additions_max;

    public String toString(){
        String msg;

        msg = this.name + "; content: ";
        for (int i = 0; i < this.content.size(); i++){
            msg += this.content.get(i).getName() + ", ";
        }
        msg += "; additions availables: ";
        for (int i = 0; i < this.additionsAvailable.size(); i++){
            msg += this.additionsAvailable.get(i).getName() + ", ";
        }
        msg += "; additions max: " + this.additions_max;
        msg += "; price: " + this.price;
        return msg;
    }

    public Burger_type(String name, ArrayList<food> content, double price, ArrayList<food> additionsAvailable, int additions_max) {
        this.name = name;
        this.content = content;
        this.price = price;
        for (int i = 0; i < content.size(); i++) {
            this.price += content.get(i).getPrice();
        }
        this.additionsAvailable = additionsAvailable;
        this.additions_max = additions_max;
    }

    public Burger_type(String name) {
        this(name, new ArrayList<food>(), 0, new ArrayList<food>(), 0);
    }


    public Burger_type() {
        this("NotSpecified", new ArrayList<food>(), 0, new ArrayList<food>(), 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<food> getContent() {
        return this.content;
    }

    public void setContent(ArrayList<food> content) {
        this.content = content;
    }

    public void AddContent(food content) {
        this.content.add(content);
    }

    public void RemoveContent(food content) {
        this.content.remove(content);
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<food> getAdditionsAvailable() {
        return additionsAvailable;
    }

    public void setAdditionsAvailable(ArrayList<food> additionsAvailable) {
        this.additionsAvailable = additionsAvailable;
    }

    public void AddAdditionsAvailable(food addition) {
        this.additionsAvailable.add(addition);
    }

    public void RemoveAdditionsAvailable(food addition) {
        this.additionsAvailable.remove(addition);
    }

    public int getAdditions_max() {
        return additions_max;
    }

    public void setAdditions_max(int additions_max) {
        this.additions_max = additions_max;
    }
}
