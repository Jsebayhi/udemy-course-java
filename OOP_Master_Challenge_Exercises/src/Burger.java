import java.util.ArrayList;

/**
 * Created by bouh on 10/01/17.
 */
public class Burger {

    private Burger_type base;
    private ArrayList<food> additionsSelected;

    public Burger() {
        this(new Burger_type(), new ArrayList<food>());
    }

    public Burger(Burger_type base, ArrayList<food> additionsSelected) {
        this.base = base;
        this.additionsSelected = additionsSelected;
    }

    public Burger(Burger_type base) {
        this(base, new ArrayList<food>());
    }

    public Burger(ArrayList<food> additionsSelected) {
        this(new Burger_type(), additionsSelected);
    }

    public ArrayList<food> getAdditionsSelected() {
        return additionsSelected;
    }

    public void setAdditionsSelected(ArrayList<food> additionsSelected) {
        this.additionsSelected = additionsSelected;
    }

    public void AddAdditionsSelected(food additionSelected) {
        this.additionsSelected.add(additionSelected);
    }

    public void DelAdditionsSelected(food additionSelected) {
        this.additionsSelected.remove(additionSelected);
    }

    public void setBase(Burger_type burgerType) {
        this.base = burgerType;
    }
}
